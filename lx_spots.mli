(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


(* Lx_spots provides an additional feature for labltk canvases: You can
 * define event handlers for rectangular regions ("spots") that are independent
 * of any canvas object. Remember that the Canvas module only allows
 * event handlers that are bound to canvas objects (Canvas.bind), and
 * the handlers for an object are only active if the object happens to
 * be visible at a certain position, and is not hidden by another
 * object.
 *
 * Spots are different. First, spots are always rectangular. Second,
 * a spot never hides another spot. When spots overlap, both spots are
 * considered as being active in the overlapping region, and the handlers
 * of both spots are called.
 *
 * Last but not least, there are no memory leaks with spots. The handlers
 * defined by Canvas.bind cannot be released individually, and will 
 * allocate memory until the whole canvas is destroyed. This makes highly
 * dynamic canvas instances impossible. In contrast to this, spots can
 * be deleted, and the memory of the released handlers will be given back
 * to the system.
 *)

type supported_event = 
  [ `Motion | `Enter | `Leave | `KeyPress | `KeyRelease | `ButtonPress
  | `ButtonRelease
  ]
  (* The event handlers that can be defined for spots *)

type spot
  (* A spot is a rectangular region of the canvas with an event
   * handler
   *)

type t
  (* A set of spots for a certain canvas *)

val attach : Widget.canvas Widget.widget -> t
  (* Creates a set of spots and attaches it to the passed canvas.
   * Event handlers for all supported_events are set.
   *)

val add :
      ?events:supported_event list ->
      ?action:(supported_event -> Tk.eventInfo -> unit) ->
      ?tags:string list ->
      t ->                          (* set of spots *)
      Tk.tagOrId ->
      (int * int * int * int) ->    (* dimension *)
        unit
  (* Adds a spot to the set of spots. The dimension of the spot is
   * specified by the quadruple (x0,y0,x1,y1) (as returned by the
   * bbox functions of labltk). The spot must have at least one tag
   * or ID for identification.
   *
   * ~tags: Additional tags
   * ~events: The events for which the action function is called
   * ~action: The action when an event happens
   *
   * Note that you cannot specify the event fields that are 
   * filled in the eventInfo record, because the module uses a fixed
   * set of fields:
   * - `State; `Time; `MouseX; `MouseY; `SendEvent; `RootWindow; `SubWindow;
   *   `Type; `Widget; `RootX; `RootY: always filled
   * - `KeyCode; `KeySymString; `KeySymInt; `Char: for key events
   * - `ButtonNumber: for button events
   *)

val change_action :
      ?events:supported_event list ->
      ?action:(supported_event -> Tk.eventInfo -> unit) ->
      t ->                          (* set of spots *)
      Tk.tagOrId list ->
	unit
  (* Sets the event list and the action function for all spots that match
   * the tagOrId criterion.
   *)

val delete : t -> Tk.tagOrId list -> unit
  (* Deletes all spots from the set that match the tagOrId criterion. *)

val move : t -> Tk.tagOrId list -> x:int -> y:int -> unit
  (* Moves all spots by ~x pixels to the right and ~y pixels to the bottom
   * that match the tagOrId criterion.
   *)

val dimension : t -> Tk.tagOrId list -> (int * int * int * int)
  (* Returns the dimension of the bounding box of the union of all spots
   * that match the tagOrId criterion.
   * This function will raise Not_found if no spot matches.
   *)

