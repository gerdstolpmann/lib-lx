(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Tk;;
open Widget;;

module Types = struct

  type 'a tree =
      { mutable node : 'a;
	mutable children : 'a tree list;
	mutable scanned : bool;
        (* - children = [] && scanned: Do not display a "+"
	 * - children = [] && not scanned: Display a "+"
	 *)
	mutable show : bool;
        (* whether to display the children or not *)
	mutable interactive : bool;
	
    (* Note: [show && not scanned] does not make sense. In this case,
     * [show] takes precedence
     *)
      }
  
  type box_case =
      No_children_box    (* non-interactive, children (if any) are hidden *)
    | Children_box       (* non-interactive, but children are shown *)
    | Plus_box           (* interactive, branch can be opened *)
    | Minus_box          (* interactive, branch can be closed *)

  type tree_address = int list

  type 'a display_item =
      ( ( (*text:*) string * (*font:*) string option * 
	  (*fg:*) color option * (*bg:*) color option * (*outl:*) color option * 
	  (*long:*) bool * image option *
	  (*events:*) Lx_spots.supported_event list
	) * 
	(*action:*) ('a tree_widget -> 'a tree -> Lx_spots.supported_event -> eventInfo -> unit)
      )


  and 'a node_info =
      { depth : int;                      (* = number of ancestor nodes *)
	mutable last_child : 'a node_info tree option;
	mutable y0 : int;
	mutable height : int;
	tag : string;                     (* for quick deletion *)
	mutable disp_tuple : 'a display_item;(* the displayed "label" *)
	mutable case : box_case;
	mutable mapped : bool;            (* whether this node is drawn *)
	mutable text_height : int;
      }
  
  and 'a tree_widget =
      { mutable display_node : 'a tree_widget -> 'a tree -> 'a display_item;
	mutable rescan_node : 'a tree_widget -> 'a tree -> unit;
	mutable requested_tree : 'a tree;
	mutable drawn_tree : 'a node_info tree option;
	canvas : canvas widget;
	spots : Lx_spots.t;
	mutable next_tag_index : int;
	mutable window_width : int;
	mutable window_height : int;
	mutable scroll_start : int;
	mutable scroll_height : int;
	foreground : color;
	background : color;
	activeforeground : color;
	activebackground : color;
	mutable font : string;
	img_plusbox : imageBitmap;
	img_plusbox_active : imageBitmap;
	img_minusbox : imageBitmap;
	img_minusbox_active : imageBitmap;
      }
end
;;

open Types;;

let equal_di (di1_left,ea1) (di2_left,ea2) =
  (di1_left = di2_left) &&
  (ea1 == ea2)         (* this is a functional value *)
;;


let almost_equal_di (di1_left,ea1) (di2_left,ea2) =
  (di1_left = di2_left)
;;


let event_list_di ((label,font,fg,bg,outl,long,img,el),ea) = el;;

let dummy_di =
  (("",None,None,None,None,false,None,[]),fun _ _ _ _ -> ());;


let string_of_case =
  function 
      Plus_box -> "[+]"
    | Minus_box -> "[-]"
    | No_children_box -> "[ ]"
    | Children_box -> "[.]"
;;


let create_box ?(click = fun () -> ()) tw case x0 y0 tag =
  (* prerr_endline ("create_box tag="^tag ^ " case=" ^ string_of_case case); *)
  let tag_box = tag ^ "box" in
  match case with
      (Plus_box|Minus_box) ->
	let normal_bitmap =
	  if case = Plus_box then tw.img_plusbox else tw.img_minusbox in
	let active_bitmap =
	  if case = Plus_box then tw.img_plusbox_active 
	  else tw.img_minusbox_active in
	let box_id =
	  Canvas.create_image ~x:(x0-5) ~y:y0 ~anchor:`W 
	  ~image:normal_bitmap
	  ~tags:[ tag; tag_box ] tw.canvas in
	Canvas.raise tw.canvas box_id;
	let _box_id = match box_id with `Id id as x -> x | _ -> assert false in
	Lx_spots.add
	  ~events:[ `Enter; `Leave; `ButtonPress ]
	  ~action:(fun ev einfo ->
		     match ev with
			 `Enter ->
			   Canvas.configure_image ~image:active_bitmap 
			     tw.canvas box_id
		       | `Leave ->
			   Canvas.configure_image ~image:normal_bitmap 
			     tw.canvas box_id
		       | `ButtonPress ->
			   if einfo.ev_ButtonNumber = 1 then click()
		       | _ -> ()
		  )
	  ~tags:[ tag; tag_box ]
	  tw.spots 
	  _box_id 
	  (Canvas.bbox tw.canvas [box_id]);

    | (No_children_box|Children_box) ->
	(* Actually do not draw any box... *)
	ignore(
	  Canvas.create_line
          ~fill:tw.foreground
	  ~xys:[ (x0,y0); (x0+5,y0) ] ~tags:[ tag; tag_box ] tw.canvas)
;;


let determine_box_case rt =
  if rt.children<>[] then begin
    if rt.interactive then begin
      if rt.show then Minus_box else Plus_box
    end
    else 
      if rt.show then Children_box else No_children_box
  end
  else begin
    if rt.interactive then begin
      if (not rt.show) && (not rt.scanned) then Plus_box else No_children_box
    end
    else
      No_children_box
  end
;;


let have_visual_children case =
  (* The criterion when to draw a vertical line *)
  match case with
      (Plus_box | No_children_box) -> false
    | (Minus_box | Children_box)   -> true
;;


let create_vertical_line tw tag x0 y0 line_dist =
  (* prerr_endline ("create_vertical_line tag=" ^ tag); *)
  let line_id =
    Canvas.create_line
    ~fill:tw.foreground
    ~xys:[ (x0+16, y0+5); (x0+16, y0+line_dist) ] 
    ~tags:[ tag; tag^"vert" ]
      tw.canvas in
  Canvas.lower tw.canvas line_id
;;


let update_vertical_line tw tag x0 y0 line_dist =
  prerr_endline ("update_vertical_line tag=" ^ tag);
  Canvas.coords_set tw.canvas (`Tag (tag^"vert"))
  ~xys:[ (x0+16, y0+5); (x0+16, y0+line_dist) ]
;;


let bind_text tw rt tag ((label,font,fg,bg,outl,long,img,el),ea) =
  if el <> [] then begin
    let action ev einfo = 
      (* prerr_endline ("action tag=" ^ tag); *)
      ea tw rt ev einfo 
    in
    let tbtag = `Tag(tag ^ "textbind") in
    (* Lx_spots.delete_spot tw.spots tbtag; *)
    Lx_spots.add
      ~events:el
      ~action
      ~tags:[ tag ]
      tw.spots
      tbtag
      (Canvas.bbox tw.canvas [tbtag]);  (* Warning: bbox is relatively slow *)
    ()
  end
;;


let rebind_text tw rt tag ((label,font,fg,bg,outl,long,img,el),ea) =
  let tbtag = `Tag(tag ^ "textbind") in
  if el = [] then 
    Lx_spots.delete tw.spots [tbtag]
  else
    let action ev einfo = 
      (* prerr_endline ("action tag=" ^ tag); *)
      ea tw rt ev einfo 
    in
    Lx_spots.change_action ~events:el ~action tw.spots [tbtag]
;;


let empty_action _ = ();;

let unbind_text tw addr tag =
  Lx_spots.delete tw.spots [`Tag(tag ^ "textbind")]
;;


let create_text tw rt tag x0 y0 ((label,font,fg,bg,outl,long,img,el),ea as di) =
  (* Returns the height of the new text box *)
  let font = match font with Some x -> x | None -> tw.font in
  let fg = match fg with Some x -> x | None -> tw.foreground in
  let x_offset, img_height =
    match img with
	Some x ->
	  let img_id =
	    Canvas.create_image ~x:(x0+10) ~y:y0 ~anchor:`Nw
	      ~image:x ~tags:[ tag; tag^"text"; tag^"textbind" ] tw.canvas in
	  let (x1,y1,x2,y2) = Canvas.bbox tw.canvas [img_id] in
	  let w = x2-x1 in
	  let h = y2-y1 in
	  (w + w/2, h)
      | None ->
	  0, 0
  in
  let text_id = 
    Canvas.create_text ~x:(x0+10+x_offset) ~y:y0 ~anchor:`Nw
    ~tags:[ tag; tag^"text"; tag^"textbind" ]
    ~text:label ~fill:fg ~font tw.canvas in
  let (x1,y1,x2,y2) = Canvas.bbox tw.canvas [text_id] in
  let x2 = if long then tw.window_width - 2 else x2 in
  begin match bg with
      Some bgcolor ->
	let rect_id =
	  Canvas.create_rectangle ~x1 ~y1 ~x2 ~y2 ~fill:bgcolor
	    ~outline:(`Color "") ~tags:[ tag; tag^"text" ] tw.canvas in
	Canvas.lower ~below:text_id tw.canvas rect_id;
    | None ->
	()
  end;
  begin match outl with
      Some outlcolor ->
	let rect_id =
	  Canvas.create_rectangle ~x1 ~y1 ~x2 ~y2 
	    ~outline:outlcolor ~tags:[ tag; tag^"text" ] tw.canvas in
	()
    | None ->
	()
  end;
  bind_text tw rt tag di;
  max (y2-y1) img_height
;;


let rec last =
  function
      [] -> raise Not_found
    | [x] -> x
    | _::l -> last l
;;


(* Collect Lx_spots.move calls with identical deltas to save time: *)

let moved_spots = ref [];;
let moved_delta = ref 0;;

let init_moves () =
  moved_spots := []
;;

let do_moves tw =
  Lx_spots.move tw.spots !moved_spots ~x:0 ~y:!moved_delta;
  moved_spots := []
;;

let reg_move tw tag delta =
  if delta <> 0 then begin
    if !moved_spots = [] then begin
      moved_spots := [tag];
      moved_delta := delta
    end
    else begin
      if !moved_delta <> delta then do_moves tw;
      moved_spots := tag :: !moved_spots;
      moved_delta := delta
    end
  end
;;


let rec new_tree (tw : 'a tree_widget) (addr : int list) (rt : 'a tree)
                 depth y0 : 'a node_info tree =
  let nti = tw.next_tag_index in
  tw.next_tag_index <- nti + 1;
  let tag = "t" ^ string_of_int nti in
  (* prerr_endline ("new_tree tag=" ^ tag); *)
  (* Create an unmapped node, and update it. *)
  let dt = (* updated later *)
    { node = { depth = depth;
	       last_child = None;
	       y0 = y0;
	       height = 0;
	       tag = tag;
	       disp_tuple = dummy_di;
	       case = No_children_box;
	       mapped = false;
	       text_height = 0;
	     };
      children = [];
      scanned = false;  (* not used *)
      show= false;      (* not used *) 
      interactive = false;   (* not used *)
    } in
  update_tree tw addr rt dt depth y0;
  dt

and update_tree (tw : 'a tree_widget) 
                (addr : int list) (rt : 'a tree) (dt : 'a node_info tree) 
                depth y0 : unit =

  (* The X position cannot change: *)
  let x0 = 16 * depth + 8 in
  let tag = dt.node.tag in
  let new_disp_tuple = tw.display_node tw rt in

  (* prerr_endline ("update_tree tag=" ^ tag); *)

  (* Update the label: *)
  let move_delta = y0 - dt.node.y0 in
  let base_height =
    if not(equal_di new_disp_tuple dt.node.disp_tuple) || not dt.node.mapped
    then begin
      (* If only the action function changes, it is sufficient to rebind the
       * text item
       *)
      if almost_equal_di new_disp_tuple dt.node.disp_tuple && dt.node.mapped 
      then begin
	(* prerr_endline "REBIND_TEXT"; *)
	let new_el = event_list_di new_disp_tuple in
	rebind_text tw rt tag new_disp_tuple;
	dt.node.text_height
      end
      else begin
	prerr_endline "REPLACE_TEXT";
	Canvas.delete tw.canvas [`Tag(tag ^ "text") ];
	Lx_spots.delete tw.spots [ `Tag(tag ^ "textbind") ];
	let h = 
	  create_text tw rt tag x0 (y0-5-move_delta) new_disp_tuple + 2 in
	  (* move_delta: The text item will be moved below by move_delta *)
	prerr_endline "/REPLACE_TEXT";
	h
      end
    end
    else
      dt.node.text_height
  in
  let case_rt = determine_box_case rt in

  (* First update children: *)
  let new_dt_children, new_height_children, last_new_dt_child =
    if have_visual_children case_rt then 
      update_children (tw,addr,y0,depth,base_height) rt.children dt.children 0 0
    else
      (dt.children, 0, dt.node.last_child)  (* will be unmapped later *)
  in
  let new_dt_children = ref new_dt_children in

  (* Update Y position if necessary: *)
  if move_delta <> 0 then begin
    Canvas.move tw.canvas (`Tag dt.node.tag) ~x:0 ~y:move_delta;
    (* Lx_spots.move tw.spots [`Tag dt.node.tag] ~x:0 ~y:move_delta; *)
    reg_move tw (`Tag dt.node.tag) move_delta;
    dt.node.y0 <- y0;
  end;

  (* Compute case_rt and case_dt. If equal, do not change box. If different,
   * delete the box (and vertical line), and create a new one
   *)
  if case_rt = dt.node.case && dt.node.mapped then begin
    (* When case_rt=Minus_box && case_dt=Minus_box:
     * Compute new line_dist and compare with old. If this value has changed,
     * update the length of the vertical line.
     *)
    if have_visual_children case_rt then begin
      let last_child =
	match last_new_dt_child with
	    Some ch -> ch
	  | None    -> assert false in
      let old_last_child =
	match dt.node.last_child with
	    Some ch -> ch
	  | None    -> assert false in
      let line_dist = base_height + new_height_children - 
		      last_child.node.height in
      let old_line_dist = dt.node.height - old_last_child.node.height in
      if line_dist <> old_line_dist then
	update_vertical_line tw tag x0 y0 line_dist
    end
  end
  else begin
    (* Delete old box, and create new one: *)
    do_moves tw;
    Canvas.delete tw.canvas [ `Tag(tag ^ "box"); `Tag(tag ^ "vert") ];
    Lx_spots.delete tw.spots [ `Tag(tag ^ "box") ];
    create_box 
      ~click:(fun () -> click_at_box tw rt dt) tw case_rt x0 y0 tag;
    if have_visual_children case_rt then begin
      let last_child =
	match last_new_dt_child with
	    Some ch -> ch
	  | None    -> assert false in
      let line_dist = base_height + new_height_children - 
		      last_child.node.height in
      create_vertical_line tw tag x0 y0 line_dist
    end;
    (* Maybe we have to unmap the children tree: *)
    if dt.node.mapped && have_visual_children dt.node.case && 
       (not (have_visual_children case_rt))
    then begin
      delete_tree tw dt.children; (* delete the old children! *)
      new_dt_children := [];
      (* Note: it is not necessary to reset last_new_dt_child *)
    end
  end;

  (* Update dt *)
  dt.children <- !new_dt_children;
  dt.node.last_child <- last_new_dt_child;
  dt.node.y0 <- y0;
  dt.node.height <- base_height + new_height_children;
  dt.node.case <- case_rt;
  dt.node.disp_tuple <- new_disp_tuple;
  dt.node.mapped <- true;
  dt.node.text_height <- base_height;

and update_children (tw,addr,y0,depth,base_height as br) 
                    rt_children dt_children pos ydelta =
 (* special recursion: if dt is shorter, then add children to dt; if rt
  * is shorter, then delete children from dt; common children are updated
  * recursively. Output is new children height.
  *)
  match (rt_children, dt_children) with
      ([], []) ->
	(* Case: same number of children in both lists *)
	let new_dt_children = [] in
	let new_children_height = ydelta in
	(new_dt_children, new_children_height, None)
    | ([], _) ->
	(* Case: dt_children is too long *)
	delete_tree tw dt_children;
	let new_dt_children = [] in
	let new_children_height = ydelta in
	(new_dt_children, new_children_height, None)
    | (rt_child::rt_children', []) ->
	(* Case: dt_children is too short *)
	let new_dt_child = 
	  new_tree 
	    tw (pos::addr) rt_child (depth+1) (y0 + base_height + ydelta) in
	let new_dt_children', new_children_height, last_new_child =
	  update_children 
	    br rt_children' [] (pos+1) (ydelta + new_dt_child.node.height) in
	(new_dt_child :: new_dt_children', 
	 new_children_height,
	 if last_new_child = None then Some new_dt_child else last_new_child
	)
    | (rt_child::rt_children', dt_child::dt_children') ->
	(* Case: update recursively *)
	update_tree 
	  tw (pos::addr) rt_child dt_child (depth+1) 
	  (y0 + base_height + ydelta);
	let new_dt_children', new_children_height, last_new_child =
	  update_children 
	    br rt_children' dt_children' (pos+1) 
	    (ydelta + dt_child.node.height) in
	(dt_child :: new_dt_children', 
	 new_children_height,
	 if last_new_child = None then Some dt_child else last_new_child
	)

and delete_tree tw dt_list =
  let tags = ref [] in
  List.iter
    (fun dt ->
       tags := `Tag(dt.node.tag) :: !tags;
       dt.node.mapped <- false;
       delete_tree tw dt.children
    )
    dt_list;
  if !tags <> [] then begin
    Canvas.delete tw.canvas !tags;
    Lx_spots.delete tw.spots !tags;
  end

and click_at_box tw rt dt =
  rt.show <- not(rt.show);
  let old_cursor = Tk.cget tw.canvas `Cursor in
  Canvas.configure ~cursor:(`Xcursor "watch") tw.canvas;
  Tk.update();
  if rt.show then begin
    try
      tw.rescan_node tw rt;
      rt.scanned <- true;
    with
	error ->
	  prerr_endline ("Uncaught exception: " ^ Printexc.to_string error)
  end;
  update tw;
  Canvas.configure ~cursor:(`Xcursor old_cursor) tw.canvas;
  (* Maybe we have to adjust the Y view: *)
  if rt.show then begin
    let (top_frac, bot_frac) = Canvas.yview_get tw.canvas in
    let top_pixel = truncate (top_frac *. (float tw.scroll_height)) + 
		    tw.scroll_start in
    let bot_pixel = truncate (bot_frac *. (float tw.scroll_height)) +
		    tw.scroll_start in
    if dt.node.y0 + dt.node.height > bot_pixel then begin
      (* We must scroll! *)
      let height = bot_pixel - top_pixel in
      let top_pixel' = dt.node.y0 + dt.node.height - 5 - height in 
      let top_pixel'' =
	if top_pixel' <= dt.node.y0 - 5 then
	  top_pixel'
	else
	  dt.node.y0 - 5 in
      let top_frac'' = (float (top_pixel'' - tw.scroll_start)) /. 
			 (float tw.scroll_height) in
      Canvas.yview tw.canvas (`Moveto top_frac'')
    end
  end;

and update tw =
  init_moves();
  prerr_endline "update_tree";
  let dt, old_height =
    match tw.drawn_tree with
	Some dt ->
	  let old_height = dt.node.height in
	  update_tree tw [] tw.requested_tree dt 0 0;
	  do_moves tw;  (* don't forget to carry out the Lx_spots moves *)
	  dt, old_height
      | None ->
	  let dt = new_tree tw [] tw.requested_tree 0 0 in
	  tw.drawn_tree <- Some dt;
	  dt, (dt.node.height + 1)
  in
  prerr_endline "/update_tree";
  if dt.node.height <> old_height then begin
    (* Update the scrollregion of the canvas: *)
    let left = 0 in
    let top = dt.node.y0 - 7 in
    let right = tw.window_width in
    let bottom = dt.node.height in
    Canvas.configure
      ~scrollregion:(left,top,right,bottom) tw.canvas;
    tw.scroll_height <- bottom - top;
    tw.scroll_start <- top
  end
;;


let minusbox_xpm =
  "#define minusbox_width 11
#define minusbox_height 11
static unsigned char minusbox_bits[] = {
   0xff, 0x07, 0x01, 0x04, 0x01, 0x04, 0x01, 0x04, 0x01, 0x04, 0xfd, 0x05,
   0x01, 0x04, 0x01, 0x04, 0x01, 0x04, 0x01, 0x04, 0xff, 0x07};
";;


let plusbox_xpm =
  "#define plusbox.xpm_width 11
#define plusbox.xpm_height 11
static unsigned char plusbox.xpm_bits[] = {
   0xff, 0x07, 0x01, 0x04, 0x21, 0x04, 0x21, 0x04, 0x21, 0x04, 0xfd, 0x05,
   0x21, 0x04, 0x21, 0x04, 0x21, 0x04, 0x01, 0x04, 0xff, 0x07};
";;


let reconfigure tw =
  Canvas.configure 
    ~width:tw.window_width 
    ~height:tw.window_height
    tw.canvas
;;

let no_action _ _ _ _ = ();;

let display_text ?font ?foreground ?background ?outline ?image ?(long = false) 
            ?(events = []) ?(action = no_action)
	    text =
  ((text, font, foreground, background, outline, long, image,
    events), 
   action
     : 'a display_item)
;;


let create ?background ?(foreground = `Black) 
           ?(activebackground = `White) ?(activeforeground = `Black)
           ?(display = fun _ _ -> display_text "") ?(rescan = fun _ _ -> ())
	   ?(width = 200) ?(height = 200) ?(font = "fixed")
	   tree
	   parent =
  let canvas = Canvas.create ?background ~width ~height parent in
  let background = 
    match background with
	Some col -> col
      | _ -> `Color(cget canvas `Background) in
  let spots = Lx_spots.attach canvas in
  let tw = { display_node = display;
	     rescan_node = rescan;
	     requested_tree = tree;
	     drawn_tree = None;
	     canvas = canvas;
	     spots = spots;
	     next_tag_index = 0;
	     window_width = width;
	     window_height = height;
	     scroll_start = 0;  (* updated later *)
	     scroll_height = 1; (* updated later *)
	     foreground = foreground;
	     background = background;
	     activeforeground = activeforeground;
	     activebackground = activebackground;
	     font = font;
	     img_plusbox = Imagebitmap.create 
			     (* ~file:"plusbox.xpm" *)
			     ~data:plusbox_xpm
			     ~foreground:foreground 
			     ~background:background ();
	     img_plusbox_active = Imagebitmap.create 
				    (* ~file:"plusbox.xpm" *)
				    ~data:plusbox_xpm
				    ~foreground:activeforeground
				    ~background:activebackground ();
	     img_minusbox = Imagebitmap.create 
			      (* ~file:"minusbox.xpm" *)
			      ~data:minusbox_xpm
			      ~foreground:foreground
			      ~background:background ();
	     img_minusbox_active = Imagebitmap.create
				     (* ~file:"minusbox.xpm" *)
				     ~data:minusbox_xpm
				     ~foreground:activeforeground
				     ~background:activebackground ();
           }
  in
  reconfigure tw;
  update tw;
  tw
;;


let configure ?display ?rescan ?width ?height ?font ?tree tw =
  let set_if f x_opt =
    match x_opt with
	None -> ()
      | Some x -> f x
  in
  set_if (fun x -> tw.display_node <- x) display;
  set_if (fun x -> tw.rescan_node <- x) rescan;
  set_if (fun x -> tw.window_width <- x) width;
  set_if (fun x -> tw.window_height <- x) height;
  set_if (fun x -> tw.font <- x) font;
  set_if (fun x -> tw.requested_tree <- x) tree;
  reconfigure tw
;;


let canvas tw = tw.canvas;;

let tree tw = tw.requested_tree;;

let lookup tw addr =
  let rec descend addr rt =
    match addr with
	[] -> rt
      | a::addr' ->
	  let child = List.nth rt.children a in (* or Not_found *)
	  descend addr' child
  in
  descend (List.rev addr) tw.requested_tree
;;

