(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Tk
open Widget


(**********************************************************************)
(* pixel index                                                        *)
(**********************************************************************)

(* The pixel index maps pixel positions to values. It is possible to
 * locate the entry in the index that is nearest to a certain pixel
 * position. One can iterate over the index entries in an interval
 * of pixels.
 *)

module Pixel_index = struct
  type 'a t =
      { mutable idx_array : (int * 'a option) array;
	mutable idx_hole_start : int;
	mutable idx_hole_end : int;
	(* The array stores pairs (pixel_key, x) in ascending pixel order.
	 * There is a hole in the array, i.e. a region of unused array elements,
	 * from idx_hole_start (incl.) to idx_hole_end (excl.). This means that
	 * only the array positions 0..idx_hole_start-1 and
	 * idx_hole_end..length-1 are actually used.
	 *
	 * The pair (pixel_key, None) must be used for the array positions in
	 * the hole. The same pair can also be used outside the hole to indicate
	 * that the entry is deleted.
	 *
	 * In the following it is distinguished between array positions and
	 * linear positions. When the entries are numbered according to the
	 * linear scheme the array positions in the hole are skipped, such
	 * that the entries have positions in the hole-less interval 0..N-1
	 * where N is the number of entries.
	 *
	 * Deleted entries are counted in the linear positioning scheme.
	 *)
      }

  let make () =
    { idx_array = Array.make 16 (0,None);
      idx_hole_start = 0;
      idx_hole_end = 16;
    }

  let search idx pix =
    (* Determines either the first element with key pix, or (if not possible) 
     * the next bigger element, or (if not possible):
     * if pix is bigger than all elements, the position after all elements;
     * if pix is smaller than all elements, the position 0.
     *
     * The function returns the linear position of the found element.
     *
     * IMPORTANT: This function returns even deleted elements (None), and does
     * not skip over them automatically!
     *)
    let hole_len = idx.idx_hole_end - idx.idx_hole_start in
    let lin_len = Array.length idx.idx_array - hole_len in
    let at lin_pos =
      if lin_pos >= idx.idx_hole_start then
	fst(idx.idx_array.(lin_pos + hole_len))
      else
	fst(idx.idx_array.(lin_pos))
    in
    let rec srch l r =
      (* precond.: pix >= at l && pix <= at r *)
      if l = r then
	l
      else
	let m = (r+l) lsr 1 in
	let at_m = at m in
	if pix <= at_m then
	  srch l m
	else
	  if l = m then
	    r
	  else
	    srch m r
    in
    if lin_len = 0 then
      0
    else
      let first = at 0 in
      let last = at (lin_len - 1) in
      if pix < first then
	0
      else
	if pix > last then
	  lin_len
	else
	srch 0 (lin_len - 1)


  let add idx pix x =
    (* Add the element x to the index at pixel position pix. If there are
     * already elements at position pix, the new element is guaranteed to
     * be inserted before the existing elements.
     *)
    (* Rearrange the array such that the new element can be put into the
     * hole of the array.
     *)
    let lin_pos = ref(search idx pix) in
    (* The element at the linear position lin_pos is equal to pix or greater
     * than x, or it does not exist
     *)
    if idx.idx_hole_end = idx.idx_hole_start then begin
      (* The array is full! *)
      (* First try to reclaim elements with value None: *)
      let k = ref 0 in
      for j = 0 to Array.length idx.idx_array - 1 do
	match idx.idx_array.(j) with
	    (_,None) ->
	      ()
	  | y ->
	      idx.idx_array.( !k ) <- y;
	      incr k
      done;
      idx.idx_hole_start <- !k;
      idx.idx_hole_end <- Array.length idx.idx_array;
      lin_pos := search idx pix;   (* search again (positions have changed) *)
      if !k = Array.length idx.idx_array then begin
	(* Nothing to reclaim: Resize the array *)
	let l = Array.length idx.idx_array in
	let l' = 2*l in
	(* prerr_endline ("INCREASING TO #" ^ string_of_int l'); *)
	let new_array = Array.make l' (0,None) in
	Array.blit idx.idx_array 0 new_array 0 !lin_pos;
	Array.blit idx.idx_array !lin_pos new_array (l' - l + !lin_pos) 
	  (l - !lin_pos);
	idx.idx_array <- new_array;
	idx.idx_hole_start <- !lin_pos;
	idx.idx_hole_end <- l' - l + !lin_pos;
      end
    end;
    
    if !lin_pos = idx.idx_hole_start then begin
      (* Optimal case: Put the new element into the hole of the array *)
      idx.idx_array.(idx.idx_hole_start) <- (pix,Some x);
      idx.idx_hole_start <- idx.idx_hole_start + 1;
    end
    else begin
      (* Move the hole first, then put the element into the hole *)
      if !lin_pos < idx.idx_hole_start then begin
	let n = idx.idx_hole_start - !lin_pos in
	let h = idx.idx_hole_end - idx.idx_hole_start in
	Array.blit idx.idx_array !lin_pos idx.idx_array (!lin_pos+h) n;
	Array.fill idx.idx_array (!lin_pos+1) (h-1) (pix,None);
	idx.idx_array.(!lin_pos) <- (pix,Some x);
	idx.idx_hole_start <- !lin_pos+1;
	idx.idx_hole_end <- !lin_pos+h
      end
      else begin
	let n = !lin_pos - idx.idx_hole_start in
	Array.blit idx.idx_array idx.idx_hole_end 
	  idx.idx_array idx.idx_hole_start n;
	idx.idx_array.(idx.idx_hole_start+n) <- (pix,Some x);
	idx.idx_hole_start <- idx.idx_hole_start+n+1;
	idx.idx_hole_end <- idx.idx_hole_end+n;
	Array.fill idx.idx_array idx.idx_hole_start 
	  (idx.idx_hole_end-idx.idx_hole_start) (pix,None);
      end
    end

  let index_of idx lin_pos =
    (* Return the array position corresponding to the passed linear position *)
    if lin_pos >= idx.idx_hole_start then
      lin_pos + (idx.idx_hole_end - idx.idx_hole_start)
    else
      lin_pos

  let delete idx pos =
    (* Mark the element at linear position [pos] as being deleted. The real
     * removal from the array will happen later as a side effect of [add].
     *)
    let x = index_of idx pos in
    idx.idx_array.(x) <- (fst(idx.idx_array.(x)),None)


  let rec get idx pos =
    (* Returns the element at linear position [pos]. The function will fail
     * if the element is deleted.
     *)
    match idx.idx_array.(index_of idx pos) with
	(_,Some x) -> x
      | (_,None)   -> failwith "Pixel_index.get"

  let exists idx pos =
    (* Returns whether the linear position [pos] is defined and there is a
     * live (non-deleted) element.
     *)
    let length = Array.length idx.idx_array - 
		 (idx.idx_hole_end - idx.idx_hole_start) in
    pos >= 0 && pos < length && snd(idx.idx_array.(index_of idx pos)) <> None

  let defined idx pos =
    (* Returns whether the linear position [pos] is defined, i.e. refers to
     * an existing or a deleted element.
     *)
    let length = Array.length idx.idx_array - 
		 (idx.idx_hole_end - idx.idx_hole_start) in
    pos >= 0 && pos < length

  let pixel idx pos =
    (* Returns the pixel position of the linear position [pos] *)
    fst(idx.idx_array.(index_of idx pos))

  let next ?(same_pixel = false) idx pos =
    (* Returns the linear position of the next existing element in the index
     * that occurs after the linear position [pos], or raises [Not_found] when
     * there is no such element.
     * ~same_pixel: if true, the next existing element with the same pixel
     * position as the current element (at [pos]) is returned, and [Not_found]
     * is also raised when the next existing element is defined for a different
     * pixel position.
     *)
    let rec go ?pix pos =
      let x = index_of idx pos in
      if x >= Array.length idx.idx_array then
	raise Not_found
      else
	match idx.idx_array.(x) with
	    (p,None) -> 
	      ( match pix with
		    None -> go (pos+1)
		  | Some q -> if p = q then go ?pix (pos+1) else raise Not_found
	      )
	  | (p,Some v) -> 
	      ( match pix with
		    None -> pos
		  | Some q -> if p = q then pos else raise Not_found
	      )
    in
    go ?pix:(if same_pixel then Some(pixel idx pos) else None) (pos+1)

  let prev idx pos =
    (* Returns the linear position of the last existing element in the index
     * that occurs before the linear position [pos], or raises [Not_found] when
     * there is no such element.
     *)
    let rec go pos =
      let x = index_of idx pos in
      if x < 0 then
	raise Not_found
      else
	match idx.idx_array.(x) with
	    (_,None) -> go (pos-1)
	  | (_,Some v) -> pos
    in
    go (pos-1)

end
;;


(**********************************************************************)
(* stripe index                                                       *)
(**********************************************************************)

(* When a spot (always rectangular) is added to the stripe index, only
 * the edges of the spot are interesting. All horizontal edges are added
 * to the index of horizontal stripes, and all vertical edges are added
 * to the index of vertical stripes.
 *
 * EXAMPLE:
 *         |                |        |             |
 *         |                |        |             |  H stripe 1
 *     ----##################--------+-------------+-----
 *         #                #        |             |
 *         #                #        |             |  H stripe 2
 *     ----#    Spot 1      #--------###############-----
 *         #                #        #             #
 *         #                #        #    Spot 2   #  H stripe 3
 *     ----##################--------#             #-----
 *         |                |        #             #  H stripe 4
 *     ----+----------------+--------###############-----
 *         |                |        |             |  H stripe 5
 *         |                |        |             |  
 *
 * V str 1      V stripe 2   V str 3    V stripe 4    V stripe 5
 *
 * In the following, the vertical stripes are preferred as we are 
 * talking about "left" and "right" edges. However, the horizontal stripes
 * work in the same way if you replace "left" by "top", and "right" by
 * "bottom".
 *)

(*----------------------------------------------------------------------*)

module type STRIPE_ELEMENT = sig
  include Set.OrderedType
  val left_edge : t -> int   
      (* The left edge of an element *)
  val right_edge : t -> int
      (* The right edge plus 1 of an element *)
end
;;

module type STRIPE = sig
  type t
  module E : STRIPE_ELEMENT
  module ESet : Set.S (* with type elt = E.t *)
  val left_edge : t -> int
      (* The left edge of a stripe *)
  val right_edge : t -> int
      (* The right edge plus 1 of a stripe *)
  val members : t -> ESet.t
      (* The members. For all members m of the stripe s:
       *   E.left_edge m <= left_edge s
       *   E.right_edge m >= right_edge s
       * There is no member such that the left or right edge of the member
       * is inside the stripe (between the edges of the stripe).
       *)
  val split : t -> int -> (t * t)
      (* split s e: returns two substripes (s1,s2) where
       *   left_edge s1 = left_edge s
       *   right_edge s1 = e
       *   left_edge s2 = e
       *   right_edge s2 = right_edge s
       * Both substripes contain all members of s.
       *)
  val join : t -> t -> t
      (* join s1 s2: returns a new stripe s where
       *   left_edge s = left_edge s1
       *   right_edge s = right_edge s2
       * It is required that
       *   right_edge s1 = left_edge s2
       *   members s1 = [] && members s2 = []
       *)
  val add : t -> E.t -> unit
      (* Adds the element to the stripe. See [members] for preconditions. *)
  val delete : t -> E.t -> unit
      (* Deletes the element from the stripe. The element is identified by
       * its physical location ("==")
       *)
  val base_stripe : unit -> t
      (* The stripe from -maxint-1 to maxint *)
end
;;

(*----------------------------------------------------------------------*)
(* There are the following possibilities for a stripe element to be a
 * member of a stripe:
 *
 * (1) The stripe element begins at the left edge of the stripe:
 *     (left edge of element = left edge of stripe)
 *          |       |
 *          ##############
 *          #       |   ...
 *          ##############
 *          |       |
 *
 * (2) The stripe element ends at the right edge of the stripe:
 *     (right edge of element = right edge of stripe)
 *          |       |
 *      #############
 *    ...   |       #
 *      #############
 *          |       |
 *
 * Of course, it is possible that (1) and (2) hold at the same time.
 *
 * (3) The stripe element begins at a previous stripe and ends at a
 *     following stripe:
 *          |       |
 *      ##################
 *     ...  |       |   ...
 *      ##################
 *          |       |
 *
 * It is not allowed that a stripe element begins inside the stripe or
 * ends inside the stripe. The algorithm must ensure that the stripes
 * are chosen such that these cases never occur:
 *          |       |                       |       |
 *          |  ###########              #########   |
 *          |  #    |   ...           ...   |   #   |
 *          |  ###########              #########   |
 *          |       |                       |       |
 *
 *         FORBIDDEN!!!                    FORBIDDEN!!!
 *)
(*----------------------------------------------------------------------*)

module Stripe_index
         (E : STRIPE_ELEMENT)
         (S : (STRIPE with module E = E)) = 
struct

  type t = S.t Pixel_index.t ;;
  (* The stripes are indexed by their left edges *)

  let make() =
    (* Creates a new index that contains exactly one stripe from the
     * lowest possible left edge to the largest possible right edge.
     *)
    let idx = Pixel_index.make() in
    let s = S.base_stripe() in
    Pixel_index.add idx (S.left_edge s) s;
    idx
      
  let nearest idx x0 =
    (* Returns the stripe containing the pixel position x0. There is always
     * such a stripe. The exact condition is that x0 >= left_edge and
     * x0 < right_edge of the stripe.
     * Two values (pos, str) are returned where pos is the linear position
     * in the index of the stripe str.
     *)
    (* Search the stripe in the pixel index and postprocess the result.
     * Cases:
     * (1) x0 happens to be the left edge of a stripe. Simply return this
     *     stripe.
     * (2) x0 is not a left edge. Because [search] returns the next higher
     *     index element, the returned stripe is the right neighbor of the
     *     searched stripe.
     * (3) x0 is not a left edge, and it is contained in the rightmost
     *     stripe. [search] returns the position of the end of the index,
     *     but the searched stripe is the last stripe.
     * It is also possible that [search] returns an index element that
     * does not exist (caused by deleted stripes).
     *)
    let pos = Pixel_index.search idx x0 in
    let this_or_previous pos =
      if Pixel_index.pixel idx pos = x0 then
	(pos, (Pixel_index.get idx pos))   (* Case (1) *)
      else
	(* because "search" returns the next bigger element: *)
	let pos' = Pixel_index.prev idx pos in   (* Case (2) *)
	(pos', Pixel_index.get idx pos')
    in
    if Pixel_index.exists idx pos then
      this_or_previous pos   (* Cases (1) or (2) *)
    else
      (* There are non-existing elements in the index: *)
      try
	(* If [pos] is not defined, it must be at the end of the index
	 * (Case (3)):
	 *)
	if not (Pixel_index.defined idx pos) then raise Not_found;
    (* It is possible that [search] returns a deleted element, but
     * there are existing elements at the same pixel position. 
     * We can just take them, and handle them as in cases (1) or (2):
     *)
    let pos' = Pixel_index.next ~same_pixel:true idx pos in
    (* or raise Not_found *)
    this_or_previous pos'   (* harmless *)
      with
	  Not_found ->
	    (* Either case (3), or: *)
	    (* [search] found only deleted elements at the pixel position.
	     * This position is contained in the stripe to the left, so
	     * we take this one: (variant of case (2))
	     *)
	    let pos'' = Pixel_index.prev idx pos in
	    (pos'', Pixel_index.get idx pos'')
	    
  let add idx el =
    (* Add the stripe element [el] to the stripe index. As a side effect,
     * zero, one, or two stripes are added to the index, depending on the
     * case.
     *)
    let x0 = E.left_edge el in
    let x1 = E.right_edge el in
    (* First handle left edge: *)
    let pos_left, s_left = nearest idx x0 in
    if S.left_edge s_left <> x0 then begin
      let (s_left_left, s_left_right) = S.split s_left x0 in
      Pixel_index.delete idx pos_left;
      Pixel_index.add idx (S.left_edge s_left) s_left_left;
      Pixel_index.add idx x0 s_left_right;
    end;
    (* Now the right edge: *)
    let pos_right, s_right = nearest idx (x1-1) in
    if S.right_edge s_right <> x1 then begin
      let (s_right_left, s_right_right) = S.split s_right x1 in
      Pixel_index.delete idx pos_right;
      Pixel_index.add idx (S.left_edge s_right) s_right_left;
      Pixel_index.add idx x1 s_right_right;
    end;
    (* Add el to all stripes between x0 and x1: *)
    let pos_left, s_left = nearest idx x0 in
    let pos = ref pos_left in
    let stripe = ref s_left in
    begin try
      while S.right_edge !stripe <= x1 do
	S.add !stripe el;
	pos := Pixel_index.next idx !pos;   (* may raise Not_found *)
	stripe := Pixel_index.get idx !pos;
      done;
    with Not_found -> ()
    end
    
  let delete idx el =
    (* Remove the stripe element [el] from the stripe index. *)
    let x0 = E.left_edge el in
    let x1 = E.right_edge el in
    (* Remove el from all stripes between x0 and x1: *)
    let pos_left, s_left = nearest idx x0 in
    let pos = ref pos_left in
    let stripe = ref s_left in
    let found_empty_stripe = ref false in
    begin try
      while S.right_edge !stripe <= x1 do
	S.delete !stripe el;
	if S.ESet.is_empty (S.members !stripe) then 
	  found_empty_stripe := true;
	pos := Pixel_index.next idx !pos;   (* may raise Not_found *)
	stripe := Pixel_index.get idx !pos;
      done;
    with Not_found -> ()
    end;
    found_empty_stripe := false;   (* TEST *)
    (* The collector is very slow, and because of this it is currently 
     * disabled. Try to find a better strategy.
     *)
    if !found_empty_stripe then begin
      (* Two adjecent empty stripes can be joined to a single empty
       * stripe. This kind of "garbage collection" is done here.
       *)
      (* Check whether stripes can be joined with previous stripes *)
      pos := (try Pixel_index.prev idx pos_left with Not_found -> pos_left);
      let last_stripe = ref (Pixel_index.get idx !pos) in
      let last_pos = ref !pos in
      let new_stripes = ref [] in
      let last_stripe_is_new = ref false in
      (* New stripes are added later, otherwise the linear positions would
       * change. Deleting stripes does not cause any problems, because
       * Pixel_index.delete defers changes of the linear positions.
       *)
      begin try
	pos := Pixel_index.next idx !pos;
	stripe := Pixel_index.get idx !pos;
	while S.left_edge !stripe <= x1 do
	  if S.ESet.is_empty(S.members !last_stripe) && 
	    S.ESet.is_empty(S.members !stripe)
	  then begin
	    let s = S.join !last_stripe !stripe in
	    if not !last_stripe_is_new then 
	      Pixel_index.delete idx !last_pos;
	    Pixel_index.delete idx !pos;
	    last_stripe := s;
	    last_pos := (-1);   (* does not matter *)
	    last_stripe_is_new := true;
	  end
	  else begin
	    if !last_stripe_is_new then 
	      new_stripes := !last_stripe :: !new_stripes;
	    last_stripe := !stripe;
	    last_pos := !pos;
	    last_stripe_is_new := false;
	  end;
	  pos := Pixel_index.next idx !pos;
	  stripe := Pixel_index.get idx !pos;
	done
      with
	  Not_found -> ()
      end;
      if !last_stripe_is_new then 
	new_stripes := !last_stripe :: !new_stripes;
      (* Now add new (joined) stripes to index: *)
      List.iter
	(fun s -> Pixel_index.add idx (S.left_edge s) s)
	!new_stripes
    end
end
;;
      
(**********************************************************************)
(* spots                                                              *)
(**********************************************************************)

type supported_event = 
  [ `Motion | `Enter | `Leave | `KeyPress | `KeyRelease | `ButtonPress
  | `ButtonRelease
  ]
;;
      
type spot =
    { int_id : int;          (* internal ID, not exported *)
      mutable x0 : int;
      mutable x1 : int;
      mutable y0 : int;
      mutable y1 : int;
      id : int option;
      tags : string list;
      mutable events : supported_event list;
      mutable action : supported_event -> eventInfo -> unit;
    }
;;

(* Now provide sets of spots: *)

module Spot = struct
  type t = spot
  let compare s1 s2 = s1.int_id - s2.int_id
end
;;

module SpotSet = Set.Make(Spot);;


(**********************************************************************)
(* stripes with spots as stripe elements                              *)
(**********************************************************************)


module Stripe (E : STRIPE_ELEMENT) = struct
  (* An implementation for STRIPE *)
  module E = E
  module ESet = SpotSet
  type t =
      { l : int;
	r : int;
	mutable members : ESet.t;
      }
  let left_edge s = s.l
  let right_edge s = s.r
  let members s = s.members

  let split s k =
    let s1 = { l = s.l; r = k; members = s.members } in
    let s2 = { l = k; r = s.r; members = s.members } in
    (s1,s2)

  let join s1 s2 =
    assert(s1.r = s2.l);
    assert(ESet.is_empty(s1.members) && ESet.is_empty(s2.members));
    { l = s1.l; r = s2.r; members = ESet.empty }

  let add s el =
    s.members <- ESet.add el s.members

  let delete s el =
    s.members <- ESet.remove el s.members

  let base_stripe () =
    { l = -max_int-1; r = max_int; members = ESet.empty }
end
;;

module V_box = struct
  (* An implementation for STRIPE_ELEMENTs of vertical stripes *)
  type t = spot
  let compare = Spot.compare
  let left_edge b = b.x0
  let right_edge b = b.x1
end
;;

module H_box = struct
  (* An implementation for STRIPE_ELEMENTs of horizontal stripes *)
  type t = spot
  let compare = Spot.compare
  let left_edge b = b.y0
  let right_edge b = b.y1
end
;;

module H_stripe = Stripe(H_box) ;;
module V_stripe = Stripe(V_box) ;;

module H_stripe_index = Stripe_index(H_box)(H_stripe);;
module V_stripe_index = Stripe_index(V_box)(V_stripe);;

(**********************************************************************)
(* canvas event management                                            *)
(**********************************************************************)

module HS = struct          (* hashed string *)
  type base = string
  type hash = int
  type t = (base * hash)

  let compare (a_s,a_h) (b_s,b_h) = 
    let d = a_h - b_h in
    if d = 0 then
      Pervasives.compare a_s b_s
    else
      d

  let mk b = (b, Hashtbl.hash b)

  let dest b = fst b
end ;;


module HI = struct          (* hashed int *)
  type base = int
  type hash = int
  type t = (base * hash)

  let compare (a_s,a_h) (b_s,b_h) = 
    let d = a_h - b_h in
    if d = 0 then
      Pervasives.compare a_s b_s
    else
      d

  let mk b = (b, Hashtbl.hash b)

  let dest b = fst b
end ;;


module SMap = Map.Make(HS) ;;
module IMap = Map.Make(HI) ;;


type t =
    { canvas : canvas widget;
      mutable current : SpotSet.t;
      mutable id_index : spot IMap.t;
      mutable tag_index : SpotSet.t SMap.t;
      mutable h_index : H_stripe_index.t;
      mutable v_index : V_stripe_index.t;
    }
;;


let locate es x y =
  (* Returns the boxes at (x,y) *)
  let _, h_stripe = H_stripe_index.nearest es.h_index y in
  let h_spots = H_stripe.members h_stripe in
  let _, v_stripe = V_stripe_index.nearest es.v_index x in
  let v_spots = V_stripe.members v_stripe in
  SpotSet.inter h_spots v_spots   (* cool, isn't it *)
;;


let callback f x y =
  try
    f x y
  with
      error ->
	prerr_endline("Uncaught exception: " ^ Printexc.to_string error);
	()
;;


let leave_event es info =
  (* prerr_endline "L"; *)
  (* Send "Leave" events to the current boxes: *)
  SpotSet.iter
    (fun b -> 
       if List.mem `Leave b.events then callback b.action `Leave info
    )
    es.current;
  es.current <- SpotSet.empty
;;


let canvas_coords es info =
  let widget_x = info.ev_MouseX in
  let widget_y = info.ev_MouseY in
  let canvas_x = Canvas.canvasx ~x:widget_x es.canvas in
  let canvas_y = Canvas.canvasy ~y:widget_y es.canvas in
  (truncate canvas_x, truncate canvas_y)
;;


let enter_event es info =
  (* prerr_endline "E"; *)
  let (x,y) = canvas_coords es info in
  let spots = locate es x y in
  SpotSet.iter
    (fun b -> 
       if List.mem `Enter b.events then callback b.action `Enter info
    )
    spots;
  es.current <- spots
;;


let ev_names = 
  [ `Motion, "Motion";
    `Enter, "Enter";
    `Leave, "Leave";
    `KeyPress, "KeyPress";
    `KeyRelease, "KeyRelease";
    `ButtonPress, "ButtonPress";
    `ButtonRelease, "ButtonRelease"
  ]



let xy_event ev es info =
  (* prerr_endline ("XY: " ^ List.assoc ev ev_names); *)
  let (x,y) = canvas_coords es info in
  (* prerr_endline ("M x=" ^ string_of_int x ^ " y=" ^ string_of_int y); *)
  
  let spots = locate es x y in
  (* Send "Enter", ev, and "Leave" events: *)
  SpotSet.iter
    (fun spot ->
       if not(SpotSet.mem spot spots) then begin
	 if List.mem `Leave spot.events then callback spot.action `Leave info
       end;
    )
    es.current;
  SpotSet.iter
    (fun spot ->
       (* prerr_endline "Loop"; *)
       if not(SpotSet.mem spot es.current) then begin
	 if List.mem `Enter spot.events then callback spot.action `Enter info
       end;
       if List.mem ev spot.events then callback spot.action ev info
    )
    spots;
  es.current <- spots;
;;


let attach canvas =
  let es = 
    { canvas = canvas;
      current = SpotSet.empty;
      id_index = IMap.empty;
      tag_index = SMap.empty;
      h_index = H_stripe_index.make();
      v_index = V_stripe_index.make();
    }
  in
  let xy_list =
    [ `State; `Time; `MouseX; `MouseY; `SendEvent; `RootWindow; `SubWindow; 
      `Type; `Widget; `RootX; `RootY ] in
  let key_list = 
    (xy_list @ [ `KeyCode; `KeySymString; `KeySymInt; `Char ]) in
  let button_list =
    (xy_list @ [ `ButtonNumber ]) in

  bind ~events:[ `Motion ] 
       ~fields:xy_list
       ~action:(xy_event `Motion es) canvas;
  bind ~events:[ `KeyPress ]
       ~fields:key_list
       ~action:(xy_event `KeyPress es) canvas;
  bind ~events:[ `KeyRelease ] 
       ~fields:key_list
       ~action:(xy_event `KeyRelease es) canvas;
  bind ~events:[ `ButtonPress ] 
       ~fields:button_list
       ~action:(xy_event `ButtonPress es) canvas;
  bind ~events:[ `ButtonRelease ] 
       ~fields:button_list
       ~action:(xy_event `ButtonRelease es) canvas;
  bind ~events:[ `Leave ] 
       ~fields:xy_list
       ~action:(leave_event es) canvas;
  bind ~events:[ `Enter ] 
       ~fields:xy_list
       ~action:(enter_event es) canvas;
  es
;;


let next_int_id = ref 0 ;;

let add ?(events = []) ?(action = fun _ _ ->()) ?(tags = [])
    es tag_or_id (x0,y0,x1,y1) =
  (* TODO: Support for `Modified *)
  let int_id = !next_int_id in
  incr next_int_id;
  let id, tags =
    match tag_or_id with
	`Id x -> (Some x, tags)
      | `Tag t -> (None, t :: tags)
  in
  let b = { x0 = x0; x1 = x1; y0 = y0; y1 = y1; id = id; tags = tags;
	    int_id = int_id;
	    events = events;
	    action = action } in
  ( match id with
	Some x -> 
	  es.id_index <- IMap.add (HI.mk x) b es.id_index
      | None -> ()
  );
  List.iter (fun tag -> 
	       let tag' = HS.mk tag in
	       let old_set = 
		 try SMap.find tag' es.tag_index 
		 with Not_found -> SpotSet.empty in
	       let new_set =
		 SpotSet.add b old_set in
	       es.tag_index <- SMap.add tag' new_set es.tag_index) tags;
  H_stripe_index.add es.h_index b;
  V_stripe_index.add es.v_index b
;;


let find_spots es tag_or_ids =
  (* prerr_endline ("FIND"); *)
  let r = ref SpotSet.empty in
  List.iter
    (function
	 `Id id -> 
	   (try 
	      r := SpotSet.add (IMap.find (HI.mk id) es.id_index) !r
	    with Not_found -> ())
       | `Tag tag -> 
	   (try 
	      r := SpotSet.union (SMap.find (HS.mk tag) es.tag_index) !r
	    with Not_found -> ())
    )
    tag_or_ids;
  (* prerr_endline ("/FIND"); *)
  !r
;;


let change_action ?(events = []) ?(action = fun _ _ ->()) es tag_or_ids =
  let spots = find_spots es tag_or_ids in
  SpotSet.iter
    (fun spot -> 
       spot.events <- events;
       spot.action <- action
    )
    spots
;;


let delete es tag_or_ids =
  let spots = find_spots es tag_or_ids in
  (* prerr_endline "DELETE"; *)
  let del_tags = ref SMap.empty in
  SpotSet.iter
    (fun spot ->
       ( match spot.id with
	     Some x -> 
	       es.id_index <- IMap.remove (HI.mk x) es.id_index;
	   | None -> ()
       );
       List.iter
	 (fun tag -> del_tags := SMap.add (HS.mk tag) () !del_tags)
	 spot.tags;
       H_stripe_index.delete es.h_index spot;
       V_stripe_index.delete es.v_index spot
    )
    spots;
  (* Now remove all [spots] from the tag index at [del_tags]: *)
  SMap.iter
    (fun hs_tag _ ->
       let old_set = 
	 try SMap.find hs_tag es.tag_index with Not_found -> SpotSet.empty in
       let new_set =
	 SpotSet.diff old_set spots in
       if SpotSet.is_empty new_set then
	 es.tag_index <- SMap.remove hs_tag es.tag_index
       else
	 es.tag_index <- SMap.add hs_tag new_set es.tag_index;
    )
    !del_tags;

  (* prerr_endline "/DELETE"; *)
;;


let move es tag_or_ids ~x ~y =
  let spots = find_spots es tag_or_ids in
  (* prerr_endline "MOVE"; *)
  SpotSet.iter
    (fun spot ->
       if y <> 0 then H_stripe_index.delete es.h_index spot;
       if x <> 0 then V_stripe_index.delete es.v_index spot;
       spot.x0 <- spot.x0 + x;
       spot.x1 <- spot.x1 + x;
       spot.y0 <- spot.y0 + y;
       spot.y1 <- spot.y1 + y;
    )
    spots;
  SpotSet.iter
    (fun spot ->
       if y <> 0 then H_stripe_index.add es.h_index spot;
       if x <> 0 then V_stripe_index.add es.v_index spot
    )
    spots;
  (* prerr_endline "/MOVE"; *)
;;


let dimension es tag_or_ids =
  let spots = find_spots es tag_or_ids in
  if SpotSet.is_empty spots then raise Not_found;
  let x0 = ref max_int in
  let x1 = ref (-max_int-1) in
  let y0 = ref max_int in
  let y1 = ref (-max_int-1) in
  SpotSet.iter
    (fun spot ->
       x0 := min !x0 spot.x0;
       y0 := min !y0 spot.y0;
       x1 := max !x1 spot.x1;
       y1 := max !y1 spot.y1;
    )
    spots;
  (!x0, !y0, !x1, !y1)
;;
  

(*
let top = openTk() in
let c = Canvas.create ~width:100 ~height:100 top in
pack [c];
let es = attach c in
let r = Canvas.create_rectangle ~x1:30 ~y1:30 ~x2:60 ~y2:70 ~fill:`Black c in
add_box ~events:[`Enter; `Leave; `ButtonPress]
  ~action:(fun ev einfo ->
	     match ev with
		 `Enter -> prerr_endline "Enter"
	       | `Leave -> prerr_endline "Leave"
	       | `ButtonPress -> prerr_endline "Click"
	       | _ -> ())
  es "R" (Canvas.bbox c [r]);
delete_box es "R";
mainLoop()
;;
*)

