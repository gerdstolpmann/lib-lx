open Widget
open Tk
open Lx_tree

let is_dir p =
  (Unix.stat p).Unix.st_kind = Unix.S_DIR
;;

let rescan_dir tw t =
  let (full_name,name) = t.node in
  let d = Unix.opendir full_name in
  let l = ref [] in
  try
    while true do 
      let e = Unix.readdir d in
      if e <> "." && e <> ".." then
	l := e :: !l 
    done; assert false
  with
      End_of_file ->
	Unix.closedir d;
	let new_list = 
	  List.map 
	    (fun e -> (Filename.concat full_name e, e)) 
	    (List.sort compare !l)
	in
	let old_list = List.map (fun n -> n.node) t.children in
	if new_list <> old_list then
	  t.children <- List.map (fun (full_e,e) -> { node = (full_e,e); 
						      children = [];
						      show = false;
						      scanned = not(is_dir full_e)
						    }
				 ) new_list
;;
	

let ls_display tw (path,name) =
  if is_dir path then
    display ~long:true ~outline:`Black ~foreground:`White ~background:`Blue ~font:"-bitstream-monospace 821-medium-r-normal-*-*-320-*-*-m-*-iso8859-1" name
  else
    display ~events:[`ButtonPress; `Enter]
            ~action:(fun addr ev einfo ->
		       let node = lookup tw addr in
		       prerr_endline("Current: " ^ fst node.node)) name
;;

let top = openTk() in
let width = 400 in
let height = 800 in
let tw = create ~display:ls_display ~rescan:rescan_dir ~width ~height
	   ~foreground:`Red
	   ~background:`Yellow
	   (* ~font:"-bitstream-monospace 821-medium-r-normal-*-*-320-*-*-m-*-iso8859-1" *)
	   { node = ("/", "/");
	     children = [];
	     show = false;
	     scanned = false;
	   }
	   top in
let canvas = canvas tw in
let sbar = Scrollbar.create ~command:(Canvas.yview canvas)
	   ~orient:`Vertical top in
Canvas.configure ~yscrollcommand:(Scrollbar.set sbar) canvas;

pack ~side:`Left [canvas];
pack ~side:`Left ~fill:`Y [ sbar];
mainLoop();;
