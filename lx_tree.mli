(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


(* Lx_tree is a tree widget for labltk. It can visualize a tree structure,
 * and you can open and close the branches of the tree. It is not necessary
 * that the tree is completely known when you call the widget; the tree can
 * be modified dynamically, and the visualization follows the changed
 * structure.
 *
 * For every node, there may be an image (optional), and a single line
 * of text. You can bind events to nodes (i.e. to the image and/or the
 * text).
 *
 * Trees are drawn in canvas widgets.
 *)

module Types : sig
  (* The types are defined in a submodule so you can "open Lx_tree.Types" *)
  type 'a tree =
      { mutable node : 'a;
	mutable children : 'a tree list;
	mutable scanned : bool;
	mutable show : bool;
	mutable interactive : bool;
      }
      (* The fundamental tree type. You can put everything into the component
       * [node]. The [children] are the ordered descendants of the node.
       * The flag [show] determines whether the children are displayed
       * (i.e. an opened branch), or not. The flag [scanned] determines how
       * a detail is displayed: When [children] is the empty list, this can
       * be interpreted in two ways. First, it can mean that there are
       * really no children ([scanned=true]). Second, it can mean that we
       * have not yet looked at the children, and if we examine them, it might
       * turn out that the tree can be continued at this point ([scanned=false]).
       *
       * Visualization:
       *
       * Case (1): show=true, children<>[]:
       *
       *  [-] <node>              ( "[-]" is a small box containing a minus sign)
       *        |
       *        +- <child1>
       *        ...
       *        +- <childN>
       * 
       * Case (2): show=true, children=[]
       *
       *  --- <node>              (i.e. no small box)
       *
       * Case (3): show=false, children<>[]
       *
       *  [+] <node>             ( "[+]" is a small box containing a plus sign)
       *
       * Case (4): show=false, children=[], scanned=true
       *
       * --- <node>              (i.e. no small box)
       *
       * Case (5): show=false, children=[], scanned=false
       *
       *  [+] <node>
       *
       * If you click at one of the small boxes, the branch is opened ("[+]"),
       * or closed ("[-]"). Without small box, you cannot click.
       *
       * Note that the topmost node is always displayed, and cannot be hidden.
       *)

  type 'a tree_widget
    (* The displayed tree *)
    
  type 'a display_item
    (* Describes how a single node is displayed *)
end    

open Types


val create : ?background : Tk.color ->
             ?foreground : Tk.color -> 
	     ?activebackground : Tk.color ->
	     ?activeforeground : Tk.color ->
	     ?display : ('a tree_widget -> 'a tree -> 'a display_item) ->
             ?rescan : ('a tree_widget -> 'a tree -> unit) ->
	     ?width : int ->
	     ?height : int ->
	     ?font : string ->
	     'a tree ->
	     'b Widget.widget ->
	       'a tree_widget
  (* Creates a new tree widget. The tree structure and the parent of the
   * canvas must be passed to this function. All other arguments are optional.
   * 
   * ~background, ~foreground: The color of the tree background, and the
   *   color of foreground drawings, respectively.
   * ~activebackground, ~activeforeground: The colors to use when an element
   *   is highlighted because the mouse pointer is over the element
   * ~display: This function is called before a node is drawn, and it determines
   *   what to draw in addition to the pure tree structure (which is drawn
   *   anyway). Note that this function is called by the [update] function
   *   for every shown node, and should be quite quick.
   * ~rescan: This function is called before a branch is opened, and it can
   *   update the [children] list.
   * ~width: The width of the tree widget. This value determines both the
   *   width of the screen window where the tree is displayed, and the logical 
   *   width of the canvas. The logical width and the screen width are always
   *   the same value, and because of this, it does make sense to have a
   *   horizontal scrollbar for the tree widget. Items beyond the right edge
   *   are simply clipped.
   *   By default the width is 200 pixels.
   * ~height: The screen height of the tree widget. This is only the height
   *   of the screen window where the tree is displayed. The logical height
   *   may a bigger value, and it is reasonable to have a vertical scrollbar
   *   to adjust the visible region of the tree.
   *   By default the height is 200 pixels.
   * ~font: This is the default font for nodes that do not specify a different
   *   one.
   *   By default, the default font is "fixed".
   *)

val configure : ?display : ('a tree_widget -> 'a tree -> 'a display_item) ->
                ?rescan : ('a tree_widget -> 'a tree -> unit) ->
	        ?width : int ->
	        ?height : int ->
	        ?font : string ->
	        ?tree : 'a tree ->
	        'a tree_widget ->
	          unit
  (* Note that it is currently impossible to change the colors.
   * [configure] does not imply [update].
   *)


val update : 'a tree_widget -> unit
  (* Updates the drawn tree such that it corresponds to the tree structure. 
   * If you change the tree, this module does not detect the changes until
   * somebody calls explicitly [update]. Normally, these calls only happen when
   * the user clicks at the small boxes that open and close branches. It 
   * might be necessary to invoke this function more often when further
   * events are defined that change attributes of the tree.
   *
   * [update] checks for changed structure (when you modify the [children]
   * component of a node), for changed flags, and it invokes the [display]
   * function for every node to see whether the image or the text must be
   * updated.
   *)

val display_text : ?font : string ->
              ?foreground : Tk.color ->
              ?background : Tk.color ->
              ?outline : Tk.color ->
              ?image : Tk.image ->
              ?long : bool ->
              ?events : Lx_spots.supported_event list ->
              ?action : ('a tree_widget ->
			 'a tree ->
			 Lx_spots.supported_event -> 
			 Tk.eventInfo -> 
			   unit) ->
              string ->
		'a display_item
  (* Determines how to display a single node. The string argument is the text
   * to display at the node. The other properties are optional:
   *
   * ~font: The font used for the text. By default, the font of the tree widget
   *   is used.
   * ~foreground: The foreground color for the text. By default, the foreground
   *   color of the tree widget is used.
   * ~background: The background color for the text. By default, the background
   *   color of the tree widget is used. (See also ~long.)
   * ~outline: The color of the border to draw around the text. By default,
   *   no such border is drawn.
   * ~long: If [false], the text widget is only as long as needed. If [true],
   *   the text widget is expanded such that the right edge is near the right
   *   edge of the canvas. The difference between [false] and [true] is
   *   only visible if a background or outline color is defined.
   * ~image: The image to display. By default, no image is displayed.
   * ~events: The events that are bound for this node.
   * ~action: The function to call as event handler.
   *)

val canvas : 'a tree_widget -> Widget.canvas Widget.widget
  (* Returns the canvas widget 
   * 
   * Note that the caller must PACK the canvas, othwerwise the whole tree
   * is invisible.
   *)

val tree : 'a tree_widget -> 'a tree
  (* Returns the tree displayed by the widget *)

(* val parent : 'a tree_widget -> 'a tree -> 'a tree *)
(* val succ : 'a tree_widget -> 'a tree -> 'a tree *)
(* val pred : 'a tree_widget -> 'a tree -> 'a tree *)

(* How to connect to a scrollbar: 
 * If [tw] is the tree widget:
 * let canvas = tw.canvas in
 * let sbar = Scrollbar.create ~command:(Canvas.yview canvas)
 *              ~orient:`Vertical top in
 * Canvas.configure ~yscrollcommand:(Scrollbar.set sbar) canvas;
 *)


